module StoryCells
  class SubHeaderCell < Cell::ViewModel
    def show
      render
    end

    def title
      params[:action] == 'search' ? 'Search' : 'Top 15 Hacker News'
    end
  end
end
