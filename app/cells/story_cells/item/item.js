class Story {
  constructor($story) {
    this.$story = $story;
    this.$linkLoadComments = $('.comments-js', this.$story);
    this.$commentsContainer = $('.comments-container-js', this.$story);
    this.bind();
  }

  bind() {
    const self = this;

    this.$linkLoadComments.on('click', () => self.loadComments());
  }

  loadComments() {
    const self = this;
    const storyId = self.$linkLoadComments.data('story-id');

    if (this.$commentsContainer.hasClass('show')) {
      self.$linkLoadComments.text('+ Show most relevant comments');
      this.$commentsContainer.collapse('toggle');
      return;
    }

    $.ajax({
      url: "/stories/comments",
      type: 'GET',
      data: { id: storyId }
    }).done(function(response) {
      self.$commentsContainer.empty().html('<hr class="row" />');

      $.each(response, function(index, object) {
        self.$commentsContainer.append(`
          <div class='container mt-3'>
            <div class='row'><p class='by'>${object.detail.by} ${object.detail.time}</p></div>
            <div class='row'><p>${object.detail.text}</p></div>
          </div>
        `);
      });

      self.$commentsContainer.collapse('toggle');
      self.$linkLoadComments.text('- Hide most relevant comments');
    });
  }
}

$(document).on('turbolinks:load', function() {
  const $stories = $('.story-js');

  $stories.each((i, element) => {
    element.story = new Story($(element));
  });
});
