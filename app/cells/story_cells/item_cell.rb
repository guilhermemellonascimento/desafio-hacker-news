module StoryCells
  class ItemCell < Cell::ViewModel
    def show
      render
    end

    def story
      model
    end

    def date
      DateTime.strptime(story[:time].to_s, '%s').strftime("%d/%m/%Y %H:%M")
    end
  end
end
