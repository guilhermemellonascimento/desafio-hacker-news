class HackerNewsService
  def initialize(api)
    @api = api
  end

  def top_stories
    hash = {}

    @api.top_stories.each do |id|
      detail = @api.get_item(id)

      hash.store(id,
        { detail: detail.with_indifferent_access,
          num_comments: detail['kids'].try(:length),
          time: detail['time'] }
      )
    end

    sort(hash)
  end

  def search(query)
    hash = {}

    @api.search(query)['hits'].each.with_index(1) do |hit, index|
      if index <= 10
        hash.store(hit['objectID'],
          { detail: { title: hit['title'], by: hit['author'], id: hit['objectID'], url: hit['url'] },
            num_comments: hit['num_comments'],
            time: DateTime.parse(hit['created_at']).to_time.to_i
          }
        )
      end
    end

    sort(hash)
  end

  def comments(story_id)
    arr = []
    story = @api.get_item(story_id)

    return if story['kids'].nil?

    story['kids'].each do |id|
      item = @api.get_item(id)

      if item['text'] && item['text'].length >= 20
        arr << {
          detail: {
            text: item['text'], by: item['by'], time: item['time']
          }
        }
      end
    end

    arr
  end

  private

  def sort(hash)
    hash.values.sort_by { |h| DateTime.strptime(h[:time].to_s,'%s') }.reverse
  end
end
