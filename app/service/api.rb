class API
  include HTTParty

  def top_stories(limit = 15)
    self.class.get(CONFIG['TOP_STORIES_URL']).take(limit)
  end

  def get_item(story_id)
    self.class.get("#{CONFIG['STORY_URL']}/#{story_id}.json").parsed_response
  end

  def search(query)
    self.class.get("#{CONFIG['SEARCH_URL']}?query=#{query}&tags=story")
  end
end
