class StoriesController < ApplicationController
  before_action :set_api_service

  def index
    @stories = @service.top_stories
  end

  def search
    @stories = @service.search(params[:query])

    render action: :index
  end

  def comments
    @comments = @service.comments(params[:id])

    render json: @comments
  end

  private

  def set_api_service
    api = API.new
    @service = HackerNewsService.new(api)
  end
end
