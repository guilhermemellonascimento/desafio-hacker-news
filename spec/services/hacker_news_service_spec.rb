require "spec_helper"

describe HackerNewsService do
  let(:api) { API.new }
  subject { HackerNewsService.new(api) }

  describe "#top_stories" do
    let(:top_stories) { File.read('spec/fixtures/top_stories.json') }
    let(:stories) { File.read('spec/fixtures/stories.json') }

    before do
      stub_request(:get, "https://hacker-news.firebaseio.com/v0/topstories.json")
        .to_return(
          status: 200,
          body: top_stories,
          headers: { "Content-Type" => "application/json" }
        )

      JSON.parse(stories).each do |s|
        stub_request(:get, "https://hacker-news.firebaseio.com/v0/item/#{s['id']}.json")
          .to_return(
            status: 200,
            body: s.to_json,
            headers: { "Content-Type" => "application/json" }
          )
      end
    end

    it "should return 15 top stories" do
      expect(subject.top_stories.length).to eq(15)
    end

    it "should return 15 top stories sorted by most recent" do
      top_stories = subject.top_stories.map { |e| e[:time] }
      sorted = JSON.parse(stories).take(15)
                .sort_by { |h| DateTime.strptime(h['time'].to_s,'%s') }
                .reverse.map { |story| story['time'] }

      expect(top_stories).to eq(sorted)
    end
  end

  describe "#search" do
    let(:search) { File.read('spec/fixtures/search.json') }

    before do
      stub_request(:get, "http://hn.algolia.com/api/v1/search_by_date?query=brazil&tags=story")
        .to_return(
          status: 200,
          body: search,
          headers: { "Content-Type" => "application/json" }
        )
    end

    it "should return 10 search entries" do
      expect(subject.search('brazil').length).to eq(10)
    end

    it "should return search entries sorted by most recent" do
      entries = subject.search('brazil').collect { |entry| entry[:detail][:id] }

       sorted = JSON.parse(search)['hits'].take(10)
          .sort_by { |hit| DateTime.parse(hit['created_at']).to_i }
          .reverse.map { |hit| hit['objectID'] }

      expect(entries).to eq(sorted)
    end
  end

  describe "#comments" do
    let(:stories) { File.read('spec/fixtures/stories.json') }
    let(:comments) { File.read('spec/fixtures/comments.json') }
    let(:story_ids) { JSON.parse(stories).map { |e| e['id'] } }

    before do
      JSON.parse(stories).each do |s|
        stub_request(:get, "https://hacker-news.firebaseio.com/v0/item/#{s['id']}.json")
          .to_return(
            status: 200,
            body: s.to_json,
            headers: { "Content-Type" => "application/json" }
          )
      end

      JSON.parse(comments).each do |c|
        stub_request(:get, "https://hacker-news.firebaseio.com/v0/item/#{c['id']}.json")
          .to_return(
            status: 200,
            body: c.to_json,
            headers: { "Content-Type" => "application/json" }
          )
      end
    end

    it "should return comments of the story with more than 20 words" do
      expect(subject.comments(story_ids.sample).length).to eq(1)
    end
  end
end
