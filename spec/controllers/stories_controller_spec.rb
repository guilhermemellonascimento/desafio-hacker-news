require "spec_helper"

describe StoriesController, type: :controller do
  describe "#index" do
    let(:top_stories) { File.read('spec/fixtures/top_stories.json') }
    let(:stories) { File.read('spec/fixtures/stories.json') }

    before do
      stub_request(:get, "https://hacker-news.firebaseio.com/v0/topstories.json")
        .to_return(
          status: 200,
          body: top_stories,
          headers: { "Content-Type" => "application/json" }
        )

      JSON.parse(stories).each do |s|
        stub_request(:get, "https://hacker-news.firebaseio.com/v0/item/#{s['id']}.json")
          .to_return(
            status: 200,
            body: s.to_json,
            headers: { "Content-Type" => "application/json" }
          )
      end

      get :index
    end

    context "Rendering stories" do
      it "responds successfully" do
        expect(response).to be_success
      end

      it "renders the index template" do
        expect(response).to render_template('index')
      end

      it "renders top stories" do
        expect(assigns(:stories).size).to be == 15
      end
    end
  end

  describe "#search" do
    let(:search) { File.read('spec/fixtures/search.json') }

    before do
      stub_request(:get, "http://hn.algolia.com/api/v1/search_by_date?query=brazil&tags=story")
        .to_return(
          status: 200,
          body: search,
          headers: { "Content-Type" => "application/json" }
        )

      get :search, params: { query: 'brazil' }
    end

    context "Rendering search" do
      it "renders search entries" do
        expect(assigns(:stories).length).to be >= 1
      end
    end
  end

  describe "#comments" do
    let(:stories) { File.read('spec/fixtures/stories.json') }
    let(:comments) { File.read('spec/fixtures/comments.json') }
    let(:story_ids) { JSON.parse(stories).map { |e| e['id'] } }

    before do
      JSON.parse(stories).each do |s|
        stub_request(:get, "https://hacker-news.firebaseio.com/v0/item/#{s['id']}.json")
          .to_return(
            status: 200,
            body: s.to_json,
            headers: { "Content-Type" => "application/json" }
          )
      end

      JSON.parse(comments).each do |c|
        stub_request(:get, "https://hacker-news.firebaseio.com/v0/item/#{c['id']}.json")
          .to_return(
            status: 200,
            body: c.to_json,
            headers: { "Content-Type" => "application/json" }
          )
      end
    end

    context "Rendering comments" do
      it "renders comments" do
        get :comments, params: { id: story_ids.sample }

        expect(assigns(:comments).length).to be >= 1
      end

      it "does not return any comments" do
        get :comments, params: { id: 11111111 }

        expect(assigns(:comments)).to be_nil
      end
    end
  end
end
