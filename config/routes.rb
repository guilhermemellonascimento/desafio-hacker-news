Rails.application.routes.draw do
  root to: 'stories#index'

  resource :stories do
    get :search
    get :comments
  end
end
